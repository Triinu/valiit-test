import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        System.out.println("Esimene ülesanne: " + Esimene.piKorda2);

        System.out.println("Teine ülesanne: " + Teine.isEqual(2,2));


        System.out.println("Kolmas ülesanne: " + Arrays.toString(Kolmas.massiiv(new String[]{"ghdf", "1234", "12", "123456"})));

        System.out.println("Neljas ülesanne: "+  + Neljas.aastad(0)+ " sajand");
        System.out.println("Neljas ülesanne: " + Neljas.aastad(1)+ " sajand");
        System.out.println("Neljas ülesanne: " + Neljas.aastad(128)+ " sajand");
        System.out.println("Neljas ülesanne: " + Neljas.aastad(598)+ " sajand");
        System.out.println("Neljas ülesanne: " + Neljas.aastad(1624)+ " sajand");
        System.out.println("Neljas ülesanne: " + Neljas.aastad(1827)+ " sajand");
        System.out.println("Neljas ülesanne: " + Neljas.aastad(1996)+ " sajand");
        System.out.println("Neljas ülesanne: " + Neljas.aastad(2017)+ " sajand");


        Country eesti = new Country(1323824, "Eesti", "eesti");
        System.out.println("Viies ülesanne: " + eesti.toString() );


    }
}
