import java.util.ArrayList;

public class Country {
    private int population;
    private String name;
    private String keeled;

    public Country(int population, String name, String keeled){
        this.population = population;
        this.name = name;
        this.keeled = keeled;
    }

    @Override
    public String toString(){
        return String.format(getName()+" populatsioon on: " +getPopulation()+" ja enim kõneldakse järgnevaid keeli: " +getKeeled());
    }
    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKeeled() {
        return keeled;
    }

    public void setKeeled(String keeled) {
        this.keeled = keeled;
    }
}
